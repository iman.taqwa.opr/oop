<?php

require_once 'Animal/inheritance/Ape.php';
require_once 'Animal/inheritance/Frog.php';

$sheep = new Animal ("shaun");
echo $sheep->getName();
echo "<br>";
echo $sheep->getLegs();
echo "<br>";
echo $sheep->getColdBlooded();
echo "<hr>";

$sungokong = new Ape ("kerasakti");
echo $sungokong->getName();
echo "<br>";
echo $sungokong->getLegs();
echo "<br>";
echo $sungokong->getColdBlooded();
echo "<br>";
$sungokong->Yell();
echo "<hr>";

$kodok = new Frog ("buduk");
echo $kodok->getName();
echo "<br>";
echo $kodok->getLegs();
echo "<br>";
echo $kodok->getColdBlooded();
echo "<br>";
$kodok->Jump();
echo "<hr>";