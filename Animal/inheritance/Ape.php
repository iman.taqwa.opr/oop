<?php
require_once 'Animal/Animal.php';

class Ape extends Animal {
    public function __construct( $name = "name", $legs = 2, $cold_blooded= "no" ) {
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }
    public function Yell() {
        echo "Yell : Auoo";
    }
}