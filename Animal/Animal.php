<?php

class Animal {
    public $name,
           $legs,
           $cold_blooded
    ;
    public function __construct( $name = "name", $legs = 4, $cold_blooded= "no" ) {
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }

    public function getName() {
        return "Name : {$this->name}";
    }
    public function getLegs() {
        return "legs : {$this->legs}";
    }
    public function getColdBlooded() {
        if ($this->cold_blooded == "no") {return "cold blooded : no";}
            else {return "cold blooded : yes";}
        ;
    }
    
}